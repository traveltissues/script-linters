FROM alpine:3.19

ARG SHELLCHECK="shellcheck=0.9.0-r4"
ARG PYLINT="py3-pylint=2.17.7-r0"

RUN apk add --no-cache $SHELLCHECK
RUN apk add --no-cache $PYLINT

CMD ["/bin/sh"]
